terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.82"
    }
  }
}

variable "prefix" {
  default = "FTP-VM"
}

variable username {
  description = "The username for the ftp server"
  type = string
  sensitive = true
}

variable password {
  description = "The password for the ftp server"
  type = string
  sensitive = true
}

provider "azurerm" {
  skip_provider_registration = true
  features {}
}

resource "azurerm_resource_group" "TestingTerraform" {
  name     = "TestingTerraform"
  location = "West Europe"
}

resource "azurerm_public_ip" "FTP-publicIp" {
  name                = "${var.prefix}-PublicIp"
  allocation_method   = "Static"
  location            = azurerm_resource_group.TestingTerraform.location
  resource_group_name = azurerm_resource_group.TestingTerraform.name
}

resource "azurerm_virtual_network" "FTP-VN" {
  name                = "${var.prefix}-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.TestingTerraform.location
  resource_group_name = azurerm_resource_group.TestingTerraform.name
}

resource "azurerm_subnet" "FTP-subnet" {
  name                 = "${var.prefix}-subnet"
  resource_group_name  = azurerm_resource_group.TestingTerraform.name
  virtual_network_name = azurerm_virtual_network.FTP-VN.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_security_group" "FTP-NSG" {
  name = "${var.prefix}-nsg"
  location = azurerm_resource_group.TestingTerraform.location
  resource_group_name = azurerm_resource_group.TestingTerraform.name
}

resource "azurerm_network_security_rule" "RDP" {
  name      = "RDP"
  priority  = 300
  protocol  = "Tcp"
  access = "Allow"
  direction = "Inbound"
  resource_group_name = azurerm_resource_group.TestingTerraform.name
  destination_address_prefix = "*"
  destination_port_range = 3389
  source_address_prefix = "*"
  source_port_range = "*"
  network_security_group_name = azurerm_network_security_group.FTP-NSG.name
}

resource "azurerm_network_security_rule" "FTP" {
  name      = "FTP_Ports"
  priority  = 310
  protocol  = "*"
  access = "Allow"
  direction = "Inbound"
  destination_address_prefix = "*"
  destination_port_ranges = ["21-22", "3000-3005"]
  source_address_prefix = "*"
  source_port_range = "*"
  resource_group_name = azurerm_resource_group.TestingTerraform.name
  network_security_group_name = azurerm_network_security_group.FTP-NSG.name
}

resource "azurerm_network_interface" "FTP-NI" {
  name                = "${var.prefix}-nic"
  location            = azurerm_resource_group.TestingTerraform.location
  resource_group_name = azurerm_resource_group.TestingTerraform.name
  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.FTP-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.FTP-publicIp.id
  }
}

resource "azurerm_storage_account" "FTP-storage" {
  name = "storageaccount"
  resource_group_name = azurerm_resource_group.TestingTerraform.name
  location = azurerm_resource_group.TestingTerraform.location
  account_tier = "Standard"
  account_replication_type = "LRS" //Locally redundant storage
  account_kind = "StorageV2"
  is_hns_enabled = "true"
}


resource "azurerm_windows_virtual_machine" "FTP-Server" {
  name                  = "${var.prefix}-ftp"
  location              = azurerm_resource_group.TestingTerraform.location
  resource_group_name   = azurerm_resource_group.TestingTerraform.name
  network_interface_ids = [azurerm_network_interface.FTP-NI.id]
  size                  = "Standard_D2s_v3"
  admin_password        = "${var.password}"
  admin_username        = "${var.username}"

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }
  os_disk {
    name                 = "ftpServer_OSDisk1"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
}
